'use strict';

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _radium = require('radium');

var _radium2 = _interopRequireDefault(_radium);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var test = "test string";

// // Radium styles
// var styles = {
// 	base: {
// 		color: 'yellow',
// 		fontWeight : 100,
// 		fontSize: 21,
// 		width: '50%',
// 		margin: '0 25%',
// 		textAlign :'center',
// 		background: 'purple',
// 		':hover': {
// 			background: 'green'
// 		}
// 	}
// };

// class Hello extends Radium(React.Component) {
// 	render() {
// 		return
// 		<div className="mytheme">
// 			<h1 style={styles.base}>Hello</h1>
// 		</div>
// 	}
// };

// ReactDOM.render(<Hello/>, document.getElementById('hello'));
//# sourceMappingURL=index.js.map
